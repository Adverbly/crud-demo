class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.string :text
      t.string :detail
      t.boolean :is_palindrome

      t.timestamps
    end
  end
end
