class Message < ApplicationRecord
  before_save {|message| message[:is_palindrome] = message[:text] == message[:text].reverse}
end
