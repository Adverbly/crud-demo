class MessagesController < ApplicationController
  before_action :current_message, only: [:show, :edit, :update, :destroy]

  def index
    @messages = Message.all
    json_response(@messages)
  end

  def show
    json_response(@message)
  end

  def new
    @message = Message.new
  end

  def create
    @message = Message.create(message_params)
    json_response(@message, :created)
  end

  def edit
  end

  def update
    @message = Message.find(params[:id])
    @message.update(message_params)

    head :no_content
  end

  def destroy
    @message.destroy

    head :no_content
  end

  private

  def message_params
      params.permit :text, :detail
  end

  def current_message
    @message = Message.find(params[:id])
  end
end
