# Message CRUD

## Local Development

```
docker-compose up -d
```

## Deployment

This project is configured with a .gitlab-ci.yml file which deploys directly from master branch directly to heroku. The heroku deployment is managed with the Procfile.

## Implementation

Simple CRUD rails app created via rails new --api

## API

for curl examples, see scripts dir

### Messages

![Sequence Diagram](docs/images/sequence.svg 'Sequence Diagram')

#### GET /messages

response 200

```json
[
  {
    "id": 1,
    "text": "asdsa",
    "detail": "",
    "is_palindrome": true,
    "created_at": "2018-12-07T02:48:20.459Z",
    "updated_at": "2018-12-07T02:49:13.422Z"
  }
]
```

#### GET /messages/:id

response 200

```json
  {
    "id": 1,
    "text": "asdsa",
    "detail": "",
    "is_palindrome": true,
    "created_at": "2018-12-07T02:48:20.459Z",
    "updated_at": "2018-12-07T02:49:13.422Z"
  }
```

#### POST /messages

response 201

```json
  {
    "id": 2,
    "text": "asdf",
    "detail": "",
    "is_palindrome": false,
    "created_at": "2018-12-07T02:48:20.459Z",
    "updated_at": "2018-12-07T02:49:13.422Z"
  }
```

#### DELETE /messages/:id

response 204
